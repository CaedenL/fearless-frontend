import { NavLink } from 'react-router-dom';

function Nav() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">Conference GO!</a>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink to="/" className="nav-link" aria-current="page">Home</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="locations/new" className="nav-link" id="new_location">New Location</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="conferences/new" className="nav-link" id="new_conference" aria-current="page">New Conference</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="presentations/new" className="nav-link" id="new_presentation" aria-current="page">New presentation</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    );
  }

  export default Nav;
