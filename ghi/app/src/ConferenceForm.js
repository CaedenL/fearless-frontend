import React, { useEffect, useState} from 'react';

function ConferenceForm() {
    const [locations, setLocation] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStartDate] = useState('');
    const [ends, setEndDate] = useState('');
    const [description, setDescription] = useState('');
    const [maxPres, setMaxPresentations] = useState('');
    const [maxAtt, setMaxAttendees] = useState('');
    const [location, selectLocation] = useState('');

    const handleName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartDate = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }

    const handleEndDate = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }

    const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handlePresentations = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleAttendees = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLoaction = (event) => {
        const value = event.target.value;
        selectLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.description = description;
        data.starts= starts;
        data.ends = ends;
        data.max_presentations = maxPres;
        data.max_attendees = maxAtt;
        data.location = location;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();

            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            selectLocation('');
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            setLocation(data.locations);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleName} value={name} placeholder="Name" required type="text" id="name" className="form-control" name="name" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartDate} value={starts} placeholder="Start Date" required type="date" id="starts" name="starts" className="form-control" />
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndDate} value={ends} placeholder="End Date" required type="date" id="ends" name="ends" className="form-control" />
                <label htmlFor="ends">End date</label>
              </div>
              <div className="form-floating mb-3">
                <textarea onChange={handleDescription} value={description} placeholder="Description" required type="textarea" rows="20" id="description" name="description" size="100" className="form-control"></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentations} value={maxPres} placeholder="Max Presentations" required type="number" id="max_presentations" name="max_presentations" min="1" className="form-control" />
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendees} value={maxAtt} placeholder="Max Attendees" required type="number" id="max_attendees" name="max_attendees" min="1" className="form-control" />
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select required onChange={handleLoaction} value={location} id="location" className="form-select" name="location">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>{ location.name }</option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
